#
# This file is part of colldbtools.
#
# colldbtools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# colldbtools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with colldbtools.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2011-2014 joaof <joaof@java.net>
#


# MySQL Workbench Plugin
# Generate DDL 
# Written in MySQL Workbench 6.0.8

from wb import *
import grt
import io
import re
import os
import datetime

ModuleInfo = DefineModule(name = "derbyddl_generator", author="joaof", version="11", description="Contains Plugin derbyddl_generator")

#
# Configure generation 
#
generationDir = 'generated'

# This plugin takes the EER Model as an argument and will appear in the Plugins -> Utilities menu
@ModuleInfo.plugin("apacheddl_generator", caption="Generate DDL", description="Generate DDL (derby)", input=[wbinputs.currentModel()], pluginMenu="Utilities")
@ModuleInfo.export(grt.INT, grt.classes.workbench_physical_Model)
def derbyddl_generator(model):
    global out_createuserdt, out_deleteuserdt
    global out_createtable, out_deletetable
    global out_createcons, out_deletecons

    catalog = grt.root.wb.doc.physicalModels[0].catalog
    schema = grt.root.wb.doc.physicalModels[0].catalog.schemata[0]

    # Create output streams
    outputdir = os.path.dirname(grt.root.wb.docPath) + '/' + generationDir
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    print('Generating SQL scripts to ' + outputdir)
    out_createuserdt = createOutputFile(outputdir + '/001_create_userdt.sql')
    out_deleteuserdt = createOutputFile(outputdir + '/999_delete_userdt.sql')
    out_createuserdt.write(u'--\n-- Tables creation for user defined types\n--\n\n')
    out_deleteuserdt.write(u'--\n-- Tables deletion for user defined types\n--\n\n')

    out_createtable = createOutputFile(outputdir + '/002_create_tables.sql')
    out_deletetable = createOutputFile(outputdir + '/998_delete_tables.sql')
    out_createtable.write(u'--\n-- Tables creation\n--\n\n')
    out_deletetable.write(u'--\n-- Tables and view deletion\n--\n\n')

    out_createcons = createOutputFile(outputdir + '/003_create_constraints.sql')
    out_deletecons = createOutputFile(outputdir + '/997_delete_constraints.sql')
    out_createcons.write(u'--\n-- Constraints creation\n--\n\n')
    out_deletecons.write(u'--\n-- Constraints deletion\n--\n\n')

    # Iterate through all user data types from schema
    for userdt in catalog.userDatatypes:
        genCreateUserDataType(userdt)
        genDropUserDataType(userdt)

    # Drop views
    out_deletetable.write(u'--\n-- View deletion\n--\n\n')
    for view in schema.views:
        genDropView(view)

    # Iterate through all tables from schema
    for table in schema.tables:
        genCreateTable(table)
        genDropTable(table)

    # Constraints
    indicesToIgnore = []
    # -- Foreign keys
    for table in schema.tables:
        if table.primaryKey:
            indicesToIgnore.append(table.primaryKey)
        for fk in table.foreignKeys:
            indicesToIgnore.append(fk.index)
            addCreateForeignKeys(table, fk)
            adddropfk(table, fk)
    # -- Indices
    for table in schema.tables:
        for ix in table.indices:
            if ix in indicesToIgnore:
                print('- Index ' + ix.name + ' ignored.')
                continue
            print('+ Index ' + ix.name + ' added as index.')
            addcreateindex(table, ix)
            adddropindex(table, ix)

    # Iterate through all views from schema
    out_createtable.write(u'--\n-- View creation\n--\n\n')
    for view in schema.views:
        addview(view)

    out_createuserdt.close()
    out_deleteuserdt.close()
    out_createtable.close()
    out_deletetable.close()
    out_createcons.close()
    out_deletecons.close()


#
# Globals
#
userTypeList = []
global out_createuserdt, out_deleteuserdt
global out_createtable, out_deletetable
global out_createcons, out_deletecons

#
# Create output file and write header
#
def createOutputFile(filepath):
    retFile = open(filepath, 'w')
    retFile.write("--\n")
    retFile.write("-- Generated on {0} from MySQL Workbench using mysql2derby_plugin, version {1}.\n".format(datetime.datetime.now(), ModuleInfo.version))
    retFile.write("--\n\n")
    return retFile
    

#
# genCreateTable
#
def genCreateTable(wbtable):
    print('[Table: ' + wbtable.name + ']')
    out_createtable.write(u'CREATE TABLE ' + wbtable.name + u'(\n')

    # Columns
    strCreateTable = ""
    pks = []
    for i,column in enumerate(wbtable.columns):
        if wbtable.isPrimaryKeyColumn(column):
            pks.append(column.name)
        addcolumn(column,wbtable.name)
        if i < len(wbtable.columns)-1 or pks:
            out_createtable.write(u',')
        out_createtable.write(u'\n')

    # Primary keys
    if pks:
        out_createtable.write(u'    PRIMARY KEY(')
        out_createtable.write(u', '.join(pks))
        out_createtable.write(u')\n')

    out_createtable.write(u');\n\n')

#
# addview
#
def addview(wbview):
    # DDL
    out_createtable.write(str(wbview.sqlDefinition))
    out_createtable.write(u'\n;\n')

#
# addcolumn
#
def addcolumn(column, tablename):
    print('  ' + column.name + ': ' + column.formattedType + '/' + column.formattedRawType)
    out_createtable.write(u'    ' + column.name)
    out_createtable.write(u' ' + mysql2DerbyDdlType(column,tablename))
    if column.autoIncrement:
        out_createtable.write(u' GENERATED ALWAYS AS IDENTITY')
    if column.isNotNull:
        out_createtable.write(u' NOT NULL')
    if column.defaultValue:
        out_createtable.write(u' DEFAULT ' + toValue(column.formattedRawType, column.defaultValue))
    if isenum(column):
        addenumfk(tablename, column.name, column.formattedRawType)

#
# Convert value to type
#
def toValue(columnType, defaultValue):
    if columnType == 'BOOLEAN':
        if defaultValue == '0':
            return u'FALSE'
        else:
            return u'TRUE'
    else:
        return defaultValue

#
# mysql2DerbyDdlType
#
def mysql2DerbyDdlType(column,tablename):
    fType = column.formattedType
    rType = column.formattedRawType
    if rType == 'BLOB':
        return u'BLOB(2G)'
    if rType == 'INT':
        return u'INTEGER'
    if rType == 'TIMESTAMP':
        return u'TIMESTAMP'
    if rType == 'DATE':
        return u'DATE'
    if rType == 'BOOLEAN' or rType == 'BOOL':
        return u'BOOLEAN'
    m = re.search('VARCHAR\(.*\)', rType) 
    if m:
        return rType
    m = re.search('CHAR\(.*\)', rType) 
    if m:
        return rType
    m = re.search('DECIMAL\(.*,.*\)', rType) 
    if m:
        return rType

    if re.search('ENUM\((.*)\)', fType):
        if re.search('ENUM\((.*)\)', rType):
            raise ValueError(u'ENUM not handled. Consider defining/using an user type.')
        else:
            if not rType in userTypeList:
                raise ValueError(u'ENUM type \'' + rType + '\' expected in user type list')
            return 'VARCHAR(255)'

    raise ValueError(u'Type \'' + rType + u'\' not handled (table \'' + tablename + u'\', column \'' + column.name + u'\')\n')


#
# addCreateForeignKeys
#
def addCreateForeignKeys(table, fk):
    print('Added foreign key \'' + fk.name + '\' for table ' + table.name)
    out_createcons.write(u'ALTER TABLE ' + table.name + u' ADD CONSTRAINT ' + fk.name + u'\n')
    out_createcons.write(u'FOREIGN KEY(')
    for i,loccol in enumerate(fk.columns):
        print("=============================")
        print(sys.version)
        out_createcons.write(str(loccol.name))
        if i < len(fk.columns)-1:
            out_createcons.write(u', ')
    out_createcons.write(u') REFERENCES ' + fk.referencedTable.name + u'(')
    for i,refcol in enumerate(fk.referencedColumns):
        out_createcons.write(str(refcol.name))
        if i < len(fk.columns)-1:
            out_createcons.write(u', ')
    out_createcons.write(u');\n\n')

def addenumfk(tablename, fkColumnName, enumname):
    print('Added enum foreign key \'' + fkColumnName + '\' for table ' + tablename)
    out_createcons.write(u'ALTER TABLE ' + tablename + u' ADD CONSTRAINT fk_' + tablename + u'_' + fkColumnName + u'_' + enumname + u'\n')
    out_createcons.write(u'FOREIGN KEY(' + fkColumnName + u') REFERENCES ' + enumtablename(enumname) + u'(value);\n\n' )
    
#
# addindex
#
def addindex(table, ix):
    print('Added index \'' + ix.name + '\' for table ' + table.name)
    addcreateindex(table, ix)
    adddropindex(ix)

#
# Indices
#

def indexName(tableName, ixName):
    return 'UIX_' + tableName + '_' + ixName

def addcreateindex(table, ix):
    out_createcons.write(u'CREATE UNIQUE INDEX ' + str(indexName(table.name, ix.name)))
    out_createcons.write(u' ON ' + table.name + u'(')
    for i, ixcol in enumerate(ix.columns):
        out_createcons.write(str(ixcol.referencedColumn.name))
        if i < len(ix.columns)-1:
            out_createcons.write(u', ')
    out_createcons.write(u');\n\n')

def adddropindex(table, ix):
    out_deletecons.write(u'DROP INDEX ' + str(indexName(table.name, ix.name)) + u';\n\n')


#
# adddrop's
#
def genDropTable(table):
    out_deletetable.write(u'DROP TABLE ' + table.name + u';\n\n')

def genDropView(view):
    out_deletetable.write(u'DROP VIEW ' + view.name + u';\n\n')

def adddropfk(table, fk):
    out_deletecons.write(u'ALTER TABLE ' + table.name + u' DROP FOREIGN KEY ' + fk.name + ';\n\n')

#
# add userdt
#

def enumtablename(enumname):
    return 'ENUM_' + enumname

def genCreateUserDataType(userdt):
    if userdt.actualType.name == 'ENUM':
        print('Added table definition for enum \'' + userdt.name + '\'')
        userTypeList.append(userdt.name) 
        out_createuserdt.write('CREATE TABLE ' + enumtablename(userdt.name) + '(\n')
        out_createuserdt.write('    value VARCHAR(255) NOT NULL,\n')
        out_createuserdt.write('    PRIMARY KEY(value)\n')
        out_createuserdt.write(');\n')
        values = re.search('ENUM\((.*)\)', userdt.sqlDefinition).group(1).split(',')
        for v in values:
            v = v.strip()
            out_createuserdt.write('INSERT INTO ENUM_' + userdt.name + '(value) VALUES(' + v + ');\n')
        out_createuserdt.write('\n')

#
#
#
def genDropUserDataType(userdt):
    if userdt.actualType.name == 'ENUM':
        print('Added drop for enum table \'' + userdt.name + '\'')
        out_deleteuserdt.write('DROP TABLE ENUM_' + userdt.name + ';\n\n')

#
# Utilities
#
def isenum(column):
    return re.search('ENUM\((.*)\)', column.formattedType)

def enumvalues(column):
    return re.search('ENUM\((.*)\)', column.formattedType).group(1)


# -------------
# - Entry point
# -------------

derbyddl_generator(grt.classes.workbench_physical_Model)
